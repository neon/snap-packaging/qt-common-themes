name: qt-common-themes
summary: All the qt (common) themes
description: |
  A snap that exports the KDE/Qt6 themes and icon themes used on various Linux distros.
base: core22
compression: lzo

grade: stable
confinement: strict
version: 1.0

slots:
  qt-6-themes:
    interface: content
    source:
      read:
        - $SNAP/usr/share/plasma/desktoptheme/breeze-dark
        - $SNAP/usr/share/plasma/desktoptheme/breeze-light
        - $SNAP/usr/share/plasma/desktoptheme/default
        - $SNAP/usr/share/plasma/desktoptheme/oxygen
        - $SNAP/usr/share/plasma/look-and-feel
        - $SNAP/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR
        - $SNAP/usr/share/QtCurve
        - $SNAP/usr/share/color-schemes
        - $SNAP/usr/share/kstyle
        - $SNAP/usr/share/Kvantum
  qt-icon-themes:
    interface: content
    source:
      read:
        - $SNAP/usr/share/icons/KDE_Classic
        - $SNAP/usr/share/icons/Oxygen_White
        - $SNAP/usr/share/icons/Oxygen_Black
        - $SNAP/usr/share/icons/Oxygen_Yellow
        - $SNAP/usr/share/icons/Oxygen_Blue
        - $SNAP/usr/share/icons/Oxygen_Zion
        - $SNAP/usr/share/icons/oxygen
  qt-sound-themes:
    interface: content
    source:
      read:
        - $SNAP/share/sounds/oxygen
        - $SNAP/share/sounds/ocean

plugs:
  kf6-core22:
    content: kf6-core22-all
    interface: content
    default-provider: kf6-core22
    target: $SNAP/kf6

environment:
    LD_LIBRARY_PATH: /snap/kf6-core22/current/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR:/snap/kf6-core22/current/usr/lib:$SNAP/usr/lib:$SNAP/lib/:$SNAP/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR:/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR:/usr/lib:/lib
    QT_PLUGIN_PATH: $SNAP/kf6/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR/qt6/plugins:$SNAP/usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR/qt6/plugins
    SNAP_DESKTOP_RUNTIME: $SNAP/kf6


parts:
  base:
    after: [utils]
    plugin: nil
    stage-snaps:
    - qt-common-themes-sdk/latest/stable
    stage:
    - -usr/lib/*/libdrm*
    - -usr/share/doc/libdrm*
    override-prime: |
        set -eux
        craftctl default
        cd $CRAFT_PRIME
        find . -type f,l -name "*.h" -exec bash -c "rm -f {}*" \;
  # Breeze: KDE's default theme
  breeze:
    after: [utils]
    plugin: nil
    build-snaps:
    - qt-common-themes-sdk
    stage:
      - usr/share/QtCurve
      - usr/share/color-schemes/Breeze*
      - usr/share/kstyle
      - usr/share/plasma/desktoptheme/default
      - usr/share/plasma/desktoptheme/breeze-dark
      - usr/share/plasma/desktoptheme/breeze-light
    override-build: |
      cp -rfv /snap/qt-common-themes-sdk/current/usr $CRAFT_PART_INSTALL
      craftctl default

  oxygen:
    after: [utils]
    plugin: nil
    build-snaps:
    - qt-common-themes-sdk
    stage:
      - usr/share/color-schemes/Oxygen*
      - usr/share/plasma/desktoptheme/oxygen
      - usr/share/plasma/look-and-feel/org.kde.oxygen
    override-build: |
      cp -rfv /snap/qt-common-themes-sdk/current/usr $CRAFT_PART_INSTALL
      craftctl default

  oxygen-icons:
    after: [utils]
    plugin: nil
    build-snaps:
    - kf6-core22
    stage:
      - usr/share/icons/oxygen
    override-build: |
      cp -rfv /snap/kf6-core22/current/usr $CRAFT_PART_INSTALL
      craftctl default
      $CRAFT_STAGE/update-icon-cache.sh $CRAFT_PART_INSTALL/share/icons

  oxygen-cursors:
    after: [utils]
    plugin: nil
    build-snaps:
    - kf6-core22
    stage:
      - usr/share/icons/Oxygen_Black
      - usr/share/icons/Oxygen_Blue
      - usr/share/icons/Oxygen_White
      - usr/share/icons/Oxygen_Yellow
      - usr/share/icons/Oxygen_Zion
    organize:
      'cursors/Oxygen/Oxygen_Black/cursors' : 'share/icons/Oxygen_Black/cursors'
      'cursors/Oxygen/Oxygen_Black/index.theme' : 'share/icons/Oxygen_Black/index.theme'
      'cursors/Oxygen/Oxygen_Blue/cursors' : 'share/icons/Oxygen_Blue/cursors'
      'cursors/Oxygen/Oxygen_Blue/index.theme' : 'share/icons/Oxygen_Blue/index.theme'
      'cursors/Oxygen/Oxygen_White/cursors' : 'share/icons/Oxygen_White/cursors'
      'cursors/Oxygen/Oxygen_White/index.theme' : 'share/icons/Oxygen_White/index.theme'
      'cursors/Oxygen/Oxygen_Yellow/cursors' : 'share/icons/Oxygen_Yellow/cursors'
      'cursors/Oxygen/Oxygen_Yellow/index.theme' : 'share/icons/Oxygen_Yellow/index.theme'
      'cursors/Oxygen/Oxygen_Zion/cursors' : 'share/icons/Oxygen_Zion/cursors'
      'cursors/Oxygen/Oxygen_Zion/index.theme' : 'share/icons/Oxygen_Zion/index.theme'
    override-build: |
      cp -rfv /snap/kf6-core22/current/usr $CRAFT_PART_INSTALL
      craftctl default
      $CRAFT_STAGE/update-icon-cache.sh $CRAFT_PART_INSTALL/share/icons

  kvantum:
    after: [utils]
    plugin: nil
    build-snaps:
    - qt-common-themes-sdk
    stage:
    - usr/share/color-schemes/Kv*
    - usr/share/icons/hicolor
    - usr/share/Kvantum
    override-build: |
      cp -rfv /snap/qt-common-themes-sdk/current/usr $CRAFT_PART_INSTALL
      craftctl default
      ls $CRAFT_STAGE/

  #KDE GTK themes

  oxygen-gtk:
    after: [utils]
    plugin: nil
    build-snaps:
    - qt-common-themes-sdk
    stage-packages:
    - libgtk2.0-0
    stage:
    - share/themes/oxygen-gtk
    - lib/gtk-2.0/2.10.0/engines/liboxygen-gtk.so
    - usr/lib/$CRAFT_ARCH_TRIPLET_BUILD_FOR
    override-build: |
      cp -rfv /snap/qt-common-themes-sdk/current/* $CRAFT_PART_INSTALL
      craftctl default
      $CRAFT_STAGE/split-gtk-theme.sh $CRAFT_PART_INSTALL

  utils:
    plugin: dump
    source: utils
    prime:
      - -*
    build-packages:
      - gtk-update-icon-cache

